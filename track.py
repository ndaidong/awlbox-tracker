#!/usr/bin/env python3

from os import path, system
from time import time, sleep
from datetime import datetime

import ujson
import psutil


STATE_FILE = '/var/tmp/awl/logs/state.json'
TRACKING_DIR = 'output'


def fround(v):
    return float('%.2f' % v)


def get_datetime(pattern='%Y-%m-%d %H:%M:%S'):
    return datetime.now().strftime(pattern)


def read_json_from_file(file_path):
    try:
        if path.exists(file_path):
            with open(file_path, 'r') as read_file:
                return ujson.load(read_file)
    except Exception as err:
        print(err)
        pass
    return {}


def get_app_stats():
    result = {
        'starting': 0,
        'running': 0,
        'stopping': 0,
        'auto': 0,
        'vlc': 0,
        'ffmpeg': 0
    }
    if not path.exists(STATE_FILE):
        return result
    data = read_json_from_file(STATE_FILE)
    state = data.get('state', {})
    apps = state.get('apps', [])
    if len(apps) == 0:
        return result
    for app in apps:
        status_text = app['status_text']
        if status_text == 'STARTING' or status_text == 'RESTARTING':
            result['starting'] += 1
        elif status_text == 'RUNNING':
            result['running'] += 1
        elif status_text == 'STOPPING':
            result['stopping'] += 1
        else:
            continue
        recog_config = app.get('recog_config', {})
        decoder = recog_config.get('camera_decoder_tool', 'auto')
        if decoder == 'auto':
            result['auto'] += 1
        elif decoder == 'vlc':
            result['vlc'] += 1
        elif decoder == 'ffmpeg':
            result['ffmpeg'] += 1
    return result


def get_cpu():
    try:
        summary = psutil.cpu_percent(interval=2, percpu=True)
        cpus = list(map(lambda n: n, summary))
        usage = 0
        num_of_core = len(cpus)
        for i in range(num_of_core):
            usage += cpus[i]
        percent = usage / (num_of_core * 100)
        val = fround(percent) * 100
        return round(val)
    except Exception as err:
        print(err)
        pass
    return 0


def get_mem():
    try:
        vm = psutil.virtual_memory()
        total = vm.total
        usage = total - vm.available
        val = fround(usage / total) * 100
        return round(val)
    except Exception as err:
        print(err)
        pass
    return 0


def update(tracking_file):
    d = get_datetime()
    c = get_cpu()
    m = get_mem()
    a = get_app_stats()
    arr = [
        d,
        c,
        m,
        a['starting'],
        a['running'],
        a['stopping'],
        a['auto'],
        a['vlc'],
        a['ffmpeg']
    ]
    print(arr)
    row = [str(k) for k in arr]
    with open(tracking_file, 'a') as fs:
        fs.write('\n' + ', '.join(row))


def init():
    if not path.exists(TRACKING_DIR):
        system('mkdir ' + TRACKING_DIR)
    tracking_file = '{}/tracking_{}.csv'.format(
        TRACKING_DIR,
        int(time()),
    )
    headers = [
        'Time',
        'CPU',
        'Memory',
        'Starting',
        'Running',
        'Stopping',
        'auto',
        'vlc',
        'ffmpeg',
    ]
    if not path.exists(tracking_file):
        header = ', '.join(headers)
        with open(tracking_file, 'w+') as fs:
            fs.write(header)
    while True:
        update(tracking_file)
        sleep(5)


if __name__ == '__main__':
    init()
